package com.realwear.ble.util

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Test

class MessageEncoderTest {

    @Test
    fun returnEncodedWhenDiffLengthIsNormal() {
        val start = 0
        val before = 0
        val difference = "a"
        val encoded : List<String> = MessageEncoder.getEncodedMessage(start, before, difference)
        assertEquals(1, encoded.size)
        assertEquals("a", encoded[0])
    }

    @Test
    fun returnTwoEncodedStringsWhenDiffLengthIsMore() {
        val start = 77
        val before = 53
        val difference = "ABCD EFGH IJKL"
        val encoded : List<String> = MessageEncoder.getEncodedMessage(start, before, difference)
        assertEquals(1, encoded.size)
        assertEquals("ABCD EFGH IJKL", encoded[0])
    }

    @Test
    fun returnTwoEncodedStringsWhenDiffLengthIsEqual() {
        val start = 77
        val before = 53
        val difference = "ABCD EFGH IJ"
        val encoded : List<String> = MessageEncoder.getEncodedMessage(start, before, difference)
        assertEquals(1, encoded.size)
        assertEquals("04D|035|ABCD EFGH IJ", encoded[0])
    }

    @Test
    fun returnTwoEncodedStringsWhenDiffLengthIs36() {
        val start = 77
        val before = 53
        val difference = "ABCD EFGH IJABCD EFGH IJABCD EFGH IJ"
        val encoded : List<String> = MessageEncoder.getEncodedMessage(start, before, difference)
        assertEquals(2, encoded.size)
        assertEquals("ABCD EFGH IJABCD EF", encoded[0])
        assertEquals("GH IJABCD EFGH IJ", encoded[1])
    }

    @Test
    fun returnTwoEncodedStringsWhenDiffLengthIs0() {
        val start = 77
        val before = 12
        val difference = ""
        val encoded : List<String> = MessageEncoder.getEncodedMessage(start, before, difference)
        assertEquals(1, encoded.size)
        assertEquals("04D|00C|", encoded[0])
    }

    @Test
    fun returnTwoEncodedStringsWhenDiffLengthIs144() {
        val start = 77
        val before = 53
        val difference = "ABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJ"
        val encoded : List<String> = MessageEncoder.getEncodedMessage(start, before, difference)
        assertEquals(12, encoded.size)
        assertEquals("04D|035|ABCD EFGH IJ", encoded[0])
        assertEquals("059|000|ABCD EFGH IJ", encoded[1])
        assertEquals("065|000|ABCD EFGH IJ", encoded[2])

        assertEquals("071|000|ABCD EFGH IJ", encoded[3])
        assertEquals("07D|000|ABCD EFGH IJ", encoded[4])
        assertEquals("089|000|ABCD EFGH IJ", encoded[5])

        assertEquals("095|000|ABCD EFGH IJ", encoded[6])
        assertEquals("0A1|000|ABCD EFGH IJ", encoded[7])
        assertEquals("0AD|000|ABCD EFGH IJ", encoded[8])

        assertEquals("0B9|000|ABCD EFGH IJ", encoded[9])
        assertEquals("0C5|000|ABCD EFGH IJ", encoded[10])
        assertEquals("0D1|000|ABCD EFGH IJ", encoded[11])
    }

    @Test
    fun returnTwoEncodedStringsWhenDiffLengthIs146() {
        val start = 77
        val before = 53
        val difference = "ABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJABCD EFGH IJAB"
        val encoded : List<String> = MessageEncoder.getEncodedMessage(start, before, difference)
        assertEquals(13, encoded.size)
        assertEquals("04D|035|ABCD EFGH IJ", encoded[0])
        assertEquals("059|000|ABCD EFGH IJ", encoded[1])
        assertEquals("065|000|ABCD EFGH IJ", encoded[2])

        assertEquals("071|000|ABCD EFGH IJ", encoded[3])
        assertEquals("07D|000|ABCD EFGH IJ", encoded[4])
        assertEquals("089|000|ABCD EFGH IJ", encoded[5])

        assertEquals("095|000|ABCD EFGH IJ", encoded[6])
        assertEquals("0A1|000|ABCD EFGH IJ", encoded[7])
        assertEquals("0AD|000|ABCD EFGH IJ", encoded[8])

        assertEquals("0B9|000|ABCD EFGH IJ", encoded[9])
        assertEquals("0C5|000|ABCD EFGH IJ", encoded[10])
        assertEquals("0D1|000|ABCD EFGH IJ", encoded[11])
        assertEquals("0DD|000|AB", encoded[12])
    }

    @Test
    fun decodeMessageWithDeleteCharacters() {
        val message = "04D|035|ABCD EFGH IJ"
        val decodedMessage = MessageEncoder.getDecodedMessage(message)
        assertNotNull(decodedMessage)
        assertEquals(77, decodedMessage.start)
        assertEquals(53, decodedMessage.before)
        assertEquals("ABCD EFGH IJ", decodedMessage.difference)
    }

    @Test
    fun decodeMessageWithOnlyDelete() {
        val message = "04D|035|"
        val decodedMessage = MessageEncoder.getDecodedMessage(message)
        assertNotNull(decodedMessage)
        assertEquals(77, decodedMessage.start)
        assertEquals(53, decodedMessage.before)
        assertEquals("", decodedMessage.difference)
    }

}