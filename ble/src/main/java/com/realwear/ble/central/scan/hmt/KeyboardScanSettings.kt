package com.realwear.ble.central.scan.hmt

import com.realwear.ble.central.scan.ScanSettings

class KeyboardScanSettings : ScanSettings {
    override fun getScanMode(): Int = android.bluetooth.le.ScanSettings.SCAN_MODE_LOW_POWER
}