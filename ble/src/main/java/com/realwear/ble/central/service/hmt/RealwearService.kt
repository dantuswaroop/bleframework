package com.realwear.ble.central.service.hmt

import android.bluetooth.BluetoothDevice
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.ble.central.SingletonHolder
import com.realwear.ble.central.service.DeviceConnectionStatus
import com.realwear.ble.central.service.RemoteService
import com.realwear.ble.central.service.Service

class RealwearService private constructor(private val remoteService: RemoteService) : Service {

    companion object : SingletonHolder<RealwearService, RemoteService>(::RealwearService)

    private val _deviceConnectionStatus = MutableLiveData<DeviceConnectionStatus>()
    private val deviceConnectionStatus = _deviceConnectionStatus as LiveData<DeviceConnectionStatus>

    override fun connect(bluetoothDevice: BluetoothDevice) {
        remoteService.connect(bluetoothDevice)
        remoteService.getDeviceConnectionUpdates().observeForever {
            _deviceConnectionStatus.postValue(it)
        }
    }

    override fun getConnectionStatus(): LiveData<DeviceConnectionStatus> {
        return deviceConnectionStatus
    }

    override fun isConnected(): Boolean {
        return remoteService.isConnected()
    }

    override fun reconnect() : Boolean {
        return remoteService.reconnect()
    }

    override fun enableNotifications(enable: Boolean) {
        remoteService.enableNotifications(enable)
    }

    override fun disconnect() {
        remoteService.disconnect()
    }

    override fun writeMessage(byteArray: ByteArray) {
        remoteService.writeMessage(byteArray)
    }
}