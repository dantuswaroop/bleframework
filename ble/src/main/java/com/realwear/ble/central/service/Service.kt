package com.realwear.ble.central.service

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData

interface Service {

    fun connect(bluetoothDevice: BluetoothDevice)

    fun enableNotifications(enable : Boolean)

    fun disconnect()

    fun writeMessage(byteArray: ByteArray)

    fun getConnectionStatus() : LiveData<DeviceConnectionStatus>

    fun isConnected(): Boolean

    fun reconnect(): Boolean
}