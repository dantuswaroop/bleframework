package com.realwear.ble.central.scan

import androidx.lifecycle.LiveData
import java.util.*

interface RemoteScanner {
    fun startScan(
        autoConnect: Boolean,
        scanMode: Int,
        deviceName: String?,
        deviceAddress: String?,
        serviceUUID: UUID?,
        manufacturerId: Int?,
        manufacturerData: ByteArray?
    )

    fun stopScan()

    fun getScanUpdates() : LiveData<DeviceScanViewState>

}
