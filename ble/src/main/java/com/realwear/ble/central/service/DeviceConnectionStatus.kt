package com.realwear.ble.central.service

import android.bluetooth.BluetoothDevice
import com.realwear.ble.central.scan.DeviceScanViewState

sealed class DeviceConnectionStatus {
    object DeviceConnected: DeviceConnectionStatus()
    object DeviceDisconnected: DeviceConnectionStatus()
    class WriteMessageSuccess: DeviceConnectionStatus()
    class WriteMessageFailure : DeviceConnectionStatus()
    class NotificationReceived(val value: ByteArray?) : DeviceConnectionStatus()
}