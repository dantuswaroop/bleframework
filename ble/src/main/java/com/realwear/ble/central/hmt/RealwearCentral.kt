package com.realwear.ble.central.hmt

import android.bluetooth.BluetoothDevice
import com.realwear.ble.central.Central
import com.realwear.ble.central.scan.Scanner
import com.realwear.ble.central.service.Service

class RealwearCentral(
    val scanner: Scanner,
    val service: Service
) : Central {

    override fun startScan() {
        scanner.startScan(true)
    }

    override fun stopScan() {
        scanner.stopScan()
    }

    override fun connect(bluetoothDevice: BluetoothDevice) {
        service.connect(bluetoothDevice)
    }

    override fun disconnect() {
        service.disconnect()
    }

    override fun writeMessage(byteArray: ByteArray) {
        service.writeMessage(byteArray)
    }
}