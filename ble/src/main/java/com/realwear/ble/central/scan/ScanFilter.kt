package com.realwear.ble.central.scan

import java.util.*

interface ScanFilter {

    fun getDeviceName() : String?

    fun getDeviceAddress() : String?

    fun getServiceUUID() : UUID?

    fun getManufacturerId() : Int?

    fun getManufacturerData() : ByteArray?

}