package com.realwear.ble.central.scan.hmt

import com.realwear.ble.Constants
import com.realwear.ble.central.scan.ScanFilter
import java.util.*

class KeyboardScanFilter : ScanFilter {
    override fun getDeviceName(): String? {
        return null
    }

    override fun getDeviceAddress(): String? {
        return null
    }

    override fun getServiceUUID(): UUID? {
        return UUID.fromString(Constants.SERVICE_UUID)
    }

    override fun getManufacturerId(): Int? {
        return null
    }

    override fun getManufacturerData(): ByteArray? {
        return null
    }
}