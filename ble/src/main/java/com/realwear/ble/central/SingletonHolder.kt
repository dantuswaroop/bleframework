/*
 * RealWear Development Software, Source Code and Object Code.
 * (C) RealWear, Inc. All rights reserved.
 *
 * Contact info@realwear.com for further information about the use of this code.
 *
 * Filename: SingletonHolder.kt
 * Class: SingletonHolder.kt
 * Author: siva
 *
 */

/*
 * RealWear Development Software, Source Code and Object Code.
 * (C) RealWear, Inc. All rights reserved.
 *
 * Contact info@realwear.com for further information about the use of this code.
 *
 * Filename: SingletonHolder.kt
 * Class: SingletonHolder.kt
 * Author: siva
 *
 */

package com.realwear.ble.central

open class SingletonHolder<out T: Any, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile
    private var instance: T? = null

    fun getInstance(arg: A): T {
        val checkInstance = instance
        if (checkInstance != null) {
            return checkInstance
        }

        return synchronized(this) {
            val checkInstanceAgain = instance
            if (checkInstanceAgain != null) {
                checkInstanceAgain
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}