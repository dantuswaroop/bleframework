package com.realwear.ble.central.scan

import android.bluetooth.BluetoothDevice

sealed class DeviceScanViewState {
    object NeedLocationPermission : DeviceScanViewState()
    object ScanStarted: DeviceScanViewState()
    object ScanAlreadyRunning: DeviceScanViewState()
    object BluetoothTurnedOff : DeviceScanViewState()
    class ScanResults(val scanResults: Map<String, BluetoothDevice>): DeviceScanViewState()
    class ScanStopped : DeviceScanViewState()
    class ScanFailed(errorMessage: String) : DeviceScanViewState()
}