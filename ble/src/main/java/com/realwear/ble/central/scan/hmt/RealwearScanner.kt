package com.realwear.ble.central.scan.hmt

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.ble.central.scan.*

class RealwearScanner(val scanFilter: ScanFilter,
                      val scanSettings: ScanSettings,
                      val remoteScanner: RemoteScanner
) : Scanner {

    // LiveData for sending the view state to the DeviceScanFragment
    private val _viewState = MutableLiveData<DeviceScanViewState>()
    private val viewState = _viewState as LiveData<DeviceScanViewState>

    override fun startScan(autoConnect: Boolean) {
        val scanMode = scanSettings.getScanMode()
        remoteScanner.startScan(autoConnect,
            scanMode, scanFilter.getDeviceName(),
            scanFilter.getDeviceAddress(),
            scanFilter.getServiceUUID(),
            scanFilter.getManufacturerId(),
            scanFilter.getManufacturerData()
        )
        remoteScanner.getScanUpdates().observeForever {
            _viewState.postValue(it)
        }
    }

    override fun getScanUpdates() : LiveData<DeviceScanViewState> {
        return viewState
    }

    override fun stopScan() {
        remoteScanner.stopScan()
    }
}