package com.realwear.ble.central.scan.hmt

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.content.pm.PackageManager
import android.os.Handler
import android.os.ParcelUuid
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.ble.central.scan.DeviceScanViewState
import com.realwear.ble.central.scan.RemoteScanner
import java.util.*

class RealwearRemoteScanner(val context: Context) : RemoteScanner {

    private val SCAN_PERIOD = 30000L
    private val TAG: String = "RealwearRemoteCentral"
    private var scanner: BluetoothLeScanner? = null
    private var adapter: BluetoothAdapter?
    private var bluetoothManager: BluetoothManager
    private var scanCallback: DeviceScanCallback? = null
    // String key is the address of the bluetooth device
    private val scanResults = mutableMapOf<String, BluetoothDevice>()

    init {
        bluetoothManager =
            context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        adapter = bluetoothManager.adapter
    }

    // LiveData for sending the view state to the DeviceScanFragment
    private val _scanState = MutableLiveData<DeviceScanViewState>()
    val scanState = _scanState as LiveData<DeviceScanViewState>

    override fun startScan(
        autoConnect: Boolean,
        scanMode: Int,
        deviceName: String?,
        deviceAddress: String?,
        serviceUUID: UUID?,
        manufacturerId: Int?,
        manufacturerData: ByteArray?
    ) {

        if (context.applicationContext.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED
        ) {
            _scanState.postValue(DeviceScanViewState.NeedLocationPermission)
            return
        } else if (!adapter!!.isEnabled) {
            _scanState.postValue(DeviceScanViewState.BluetoothTurnedOff)
            return
        }

       if (scanCallback == null) {
            scanner = adapter!!.bluetoothLeScanner
            Log.d(TAG, "Start Scanning")
            // Update the UI to indicate an active scan is starting
            _scanState.value = DeviceScanViewState.ScanStarted

            // Stop scanning after the scan period
            Handler().postDelayed({ stopScanning() }, SCAN_PERIOD)

            // Kick off a new scan
            scanCallback = DeviceScanCallback()
           val filterBuilder = ScanFilter.Builder()
           deviceName?.let {
               filterBuilder.setDeviceName(it)
           }
           deviceAddress?.let {
               filterBuilder.setDeviceAddress(it)
           }
           serviceUUID?.let {
               filterBuilder.setServiceUuid(ParcelUuid( it))
           }
           manufacturerId?.let {
               filterBuilder.setManufacturerData(it, manufacturerData)
           }
           val scanSettings = ScanSettings.Builder().setScanMode(scanMode).build()
           scanner?.startScan(listOf(filterBuilder.build()), scanSettings, scanCallback)
        } else {
            Log.d(TAG, "Already scanning")
           _scanState.value = DeviceScanViewState.ScanAlreadyRunning
        }

    }

    private fun stopScanning() {
        Log.d(TAG, "Stopping Scanning")
        scanner?.stopScan(scanCallback)
        scanCallback = null
        // return the current results
        _scanState.value = DeviceScanViewState.ScanResults(scanResults)
        _scanState.value = DeviceScanViewState.ScanStopped()
    }

    override fun stopScan() {
        stopScanning()
    }

    override fun getScanUpdates(): LiveData<DeviceScanViewState> {
        return scanState
    }

    /**
     * Custom ScanCallback object - adds found devices to list on success, displays error on failure.
     */
    private inner class DeviceScanCallback : ScanCallback() {
        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)
            for (item in results) {
                val realwearID : String = String(item.scanRecord!!.manufacturerSpecificData[1])
                println("device address " + realwearID.hashCode())

                item.device?.let { device ->
                    scanResults[realwearID] = device
                }
            }
            _scanState.value = DeviceScanViewState.ScanResults(scanResults)
        }

        override fun onScanResult(
            callbackType: Int,
            result: ScanResult
        ) {
            super.onScanResult(callbackType, result)
//            val string = String(result.scanRecord!!.manufacturerSpecificData[1])
//            ChatServer.realwearIDMap[result.device.address] = string
//            val checkForReconnection = ChatServer.checkForReconnection(result)
//            if (checkForReconnection) {
//                stopScanning()
//                _viewState.value = DeviceScanViewState.DeviceConnected
//            }
            val realwearID : String = String(result.scanRecord!!.manufacturerSpecificData[1])
            println("device address " + realwearID.hashCode())
            result.device?.let { device ->
                scanResults[realwearID] = device
            }
            _scanState.value = DeviceScanViewState.ScanResults(scanResults)
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            // Send error state to the fragment to display
            val errorMessage = "Scan failed with error: $errorCode"
            _scanState.value = DeviceScanViewState.ScanFailed(errorMessage)
        }
    }
}