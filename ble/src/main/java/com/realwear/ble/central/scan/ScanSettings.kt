package com.realwear.ble.central.scan

interface ScanSettings {

    fun getScanMode() : Int
}