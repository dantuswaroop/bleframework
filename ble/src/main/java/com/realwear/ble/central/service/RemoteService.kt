package com.realwear.ble.central.service

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData

interface RemoteService {

    fun connect(bluetoothDevice: BluetoothDevice)

    fun disconnect()

    fun writeMessage(byteArray: ByteArray)

    fun getDeviceConnectionUpdates() : LiveData<DeviceConnectionStatus>

    fun isConnected(): Boolean

    fun reconnect() : Boolean

    fun enableNotifications(enable: Boolean)
}