package com.realwear.ble.central.model

data class BTKeyboardEvent(val text: String,
                           val imeOptions : Int = -1)