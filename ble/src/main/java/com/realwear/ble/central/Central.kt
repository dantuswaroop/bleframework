package com.realwear.ble.central

import android.bluetooth.BluetoothDevice

interface Central {

    fun startScan()

    fun stopScan()

    fun connect(bluetoothDevice: BluetoothDevice)

    fun disconnect()

    fun writeMessage(byteArray: ByteArray)

}