package com.realwear.ble.central.service.hmt

import android.bluetooth.*
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.peel.prefs.SharedPrefs
import com.realwear.ble.Constants
import com.realwear.ble.central.SingletonHolder
import com.realwear.ble.central.service.DeviceConnectionStatus
import com.realwear.ble.central.service.RemoteService
import com.realwear.ble.json.AppKeys
import java.util.*

class RealwearRemoteService private constructor(val context: Context) : RemoteService {
    companion object : SingletonHolder<RealwearRemoteService, Context>(::RealwearRemoteService)

    private val STATE_DISCONNECTED = 0
    private val STATE_CONNECTING = 1
    private val STATE_CONNECTED = 2

    private var connectionState = STATE_DISCONNECTED

    private var gatt: BluetoothGatt? = null
    private var gattClient: BluetoothGatt? = null
    private var gattClientCallback: GattClientCallback = GattClientCallback()
    private var writeCharacterstic: BluetoothGattCharacteristic? = null
    private var readCharacterstic: BluetoothGattCharacteristic? = null
    private val TAG: String = "RealwareRemoteService"


    private val _deviceConnectionStatus = MutableLiveData<DeviceConnectionStatus>()
    val deviceConnectionStatus = _deviceConnectionStatus as LiveData<DeviceConnectionStatus>

    init {

    }

    override fun getDeviceConnectionUpdates(): LiveData<DeviceConnectionStatus> {
        return deviceConnectionStatus
    }

    override fun connect(bluetoothDevice: BluetoothDevice) {
        if (gatt != null) {
            if (gatt!!.connect()) {
                connectionState = STATE_CONNECTED
            }
        } else {
            Log.v(TAG, " test connecting to :: " + bluetoothDevice.address)
            if (!bluetoothDevice.createBond()) {
                gattClient = bluetoothDevice.connectGatt(
                    context,
                    false,
                    gattClientCallback,
                    BluetoothDevice.TRANSPORT_LE
                )
            }
            connectionState = STATE_CONNECTING
        }
    }

    override fun disconnect() {

        gattClient?.let {
            it.close()
            connectionState = STATE_DISCONNECTED
            gattClient = null
        }
        gatt?.let {
            it.close()
            gatt = null
            connectionState = STATE_DISCONNECTED
        }
    }

    override fun writeMessage(byteArray: ByteArray) {
        writeCharacterstic?.let { characteristic ->
            characteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE

            characteristic.value = byteArray
            gatt?.let { it ->
                val success = it.writeCharacteristic(writeCharacterstic)
                if (success) {
                    _deviceConnectionStatus.postValue(DeviceConnectionStatus.WriteMessageSuccess())
                } else {
                    _deviceConnectionStatus.postValue(DeviceConnectionStatus.WriteMessageFailure())
                }
            } ?: run {
                Log.e(TAG, "gatt is null")
                _deviceConnectionStatus.postValue(DeviceConnectionStatus.WriteMessageFailure())
            }
        }
    }

    private inner class GattClientCallback : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            val isSuccess = status == BluetoothGatt.GATT_SUCCESS
            val isConnected = newState == BluetoothProfile.STATE_CONNECTED
            Log.d(
                TAG,
                "onConnectionStateChange: Client $gatt  success: $isSuccess connected: $isConnected"
            )
            // try to send a message to the other device as a test
            if (isSuccess && isConnected) {
                // discover services
//                val realWearID = realwearIDMap[gatt.device.address]
//                if (realWearID != null) {
//                    SharedPrefs.put(AppKeys.CONNECTED_BT_ID, realWearID)
//                }
                _deviceConnectionStatus.postValue(DeviceConnectionStatus.DeviceConnected)
                connectionState = STATE_CONNECTED
                gatt.discoverServices()
            } else {
                connectionState = STATE_DISCONNECTED
                _deviceConnectionStatus.postValue(DeviceConnectionStatus.DeviceDisconnected)
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, status)

        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            Log.v(TAG, "Received Keycode " + String(characteristic!!.value) + " object :: " + this.hashCode())
            _deviceConnectionStatus.postValue(DeviceConnectionStatus.NotificationReceived(characteristic!!.value))
        }

        override fun onServicesDiscovered(discoveredGatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(discoveredGatt, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "onServicesDiscovered: Have gatt $discoveredGatt")
                gatt = discoveredGatt
                val service = discoveredGatt.getService(UUID.fromString(Constants.SERVICE_UUID))
                if (service != null) {
                    readCharacterstic =
                        service.getCharacteristic(UUID.fromString(Constants.CHARACTERISTIC_KEY_EVENT_UUID))
                    writeCharacterstic =
                        service.getCharacteristic(UUID.fromString(Constants.CHARACTERISTIC_IME_OPTIONS_UUID))
                    enableNotification(true)
                }
            }
        }
    }

    private fun enableNotification(enable: Boolean) {
        gatt?.let {
            gatt!!.setCharacteristicNotification(readCharacterstic, enable)
            val uuid: UUID =
                UUID.fromString(Constants.CLIENT_CHARACTERISCIC_CONFIGURATION_DESCRIPTOR_UUID)
            val descriptor: BluetoothGattDescriptor = readCharacterstic!!.getDescriptor(uuid)
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt!!.writeDescriptor(descriptor)
        }
    }

    override fun isConnected() : Boolean {
        return connectionState == STATE_CONNECTED
    }

    override fun enableNotifications(enable: Boolean) {
        if (connectionState == STATE_CONNECTED) {
            enableNotification(enable)
        }
    }

    override fun reconnect() : Boolean {
        if (gatt != null) {
            if (gatt!!.connect()) {
                connectionState = STATE_CONNECTED
                enableNotification(true)
                return true
            }
        } else {
            val bluetoothManager =
                context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val get = SharedPrefs.get(AppKeys.CONNECTED_BT_ID, "")
            val remoteDevice = bluetoothManager.adapter.getRemoteDevice(get)
            if (remoteDevice != null) {
                connect(remoteDevice)
            }
            bluetoothManager.adapter.bondedDevices.forEach {
                val remoteDevice = bluetoothManager.adapter.getRemoteDevice(it.address)
                if (remoteDevice != null) {
                    connect(remoteDevice)
                }
            }
        }
        return false
    }
}