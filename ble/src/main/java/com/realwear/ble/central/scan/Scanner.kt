package com.realwear.ble.central.scan

import androidx.lifecycle.LiveData

interface Scanner {

    fun startScan(autoConnect : Boolean)

    fun stopScan()

    fun getScanUpdates(): LiveData<DeviceScanViewState>
}