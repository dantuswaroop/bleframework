package com.realwear.ble.peripheral.keyboard

import android.bluetooth.BluetoothGattCharacteristic
import com.realwear.ble.Constants
import com.realwear.ble.peripheral.Characteristic
import java.util.*

class KeyCharacteristic : Characteristic {

    override fun getUUID(): UUID {
        return UUID.fromString(Constants.CHARACTERISTIC_KEY_EVENT_UUID)
    }

    override fun getProperties(): Int {
        return BluetoothGattCharacteristic.PROPERTY_READ or
                BluetoothGattCharacteristic.PROPERTY_NOTIFY
    }

    override fun getPermissions(): Int {
        return BluetoothGattCharacteristic.PERMISSION_READ
    }
}