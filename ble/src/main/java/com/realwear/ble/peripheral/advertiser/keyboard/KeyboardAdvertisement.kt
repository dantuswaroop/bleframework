package com.realwear.ble.peripheral.advertiser.keyboard

import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.ble.peripheral.advertiser.*

class KeyboardAdvertisement(
    val context: Context,
    val advertisementSettings: AdvertisementSettings,
    val advertisementData: AdvertisementData,
    val remoteAdvertiser: RemoteAdvertiser
) : Advertiser {

    private val _advertisementStatus = MutableLiveData<AdvertisementStatus>()
    private val advertisementStatus = _advertisementStatus as LiveData<AdvertisementStatus>

    override fun advertise() {
        val mAdvSettings = AdvertiseSettings.Builder()
            .setAdvertiseMode(advertisementSettings.getAdvertiseMode())
            .setTxPowerLevel(advertisementSettings.getTxPowerLevel())
            .setConnectable(advertisementSettings.isConnectable())
            .build()

        val mAdvData = AdvertiseData.Builder()
            .setIncludeTxPowerLevel(advertisementData.getIncludeTxPowerLevel())
            .addServiceUuid(advertisementData.getServiceUUID())
            .build()

        val mAdvScanResponse = AdvertiseData.Builder()
            .setIncludeDeviceName(advertisementData.includeDeviceName())
//            .addManufacturerData(1, advertisementData.getRealWearID().toByteArray())
            .build()

        remoteAdvertiser.advertise(mAdvSettings, mAdvData, mAdvScanResponse)

        remoteAdvertiser.getAdvertisementCallBack().observeForever {
            _advertisementStatus.postValue(it)
        }
    }

    override fun stopAdvertising() {
        remoteAdvertiser.stopAdvertising()
    }

    override fun getAdvertisementStatus(): LiveData<AdvertisementStatus> {
        return advertisementStatus
    }

}