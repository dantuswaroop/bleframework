package com.realwear.ble.peripheral.advertiser

import androidx.lifecycle.LiveData

interface Advertiser {
    fun advertise()
    fun stopAdvertising()
    fun getAdvertisementStatus() : LiveData<AdvertisementStatus>
}