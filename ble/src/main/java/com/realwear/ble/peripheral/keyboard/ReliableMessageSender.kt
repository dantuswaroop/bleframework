package com.realwear.ble.peripheral.keyboard

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattServer
import android.util.Log
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.Executors

open class ReliableMessageSender(val keyEventCharacteristic: BluetoothGattCharacteristic?,
                                 val gattServer: BluetoothGattServer, val  mBluetoothDevices: HashSet<BluetoothDevice>) {

    val singleThreadExecutor = Executors.newSingleThreadExecutor()
    private val queue: Queue<String> = ConcurrentLinkedQueue()

    @Volatile
    private var running = false
    private val lock = Object()

    fun enqueue(requestData: String): Boolean {
        val success = queue!!.offer(requestData)
        requestMissingPacketsConsumerToProcessQueue()
        return success
    }

    private fun requestMissingPacketsConsumerToProcessQueue() {
        synchronized(lock) {
            if (!running) {
                startMissingPacketsConsumer()
            }
            lock.notifyAll()
        }
    }

    protected fun startMissingPacketsConsumer() {
        singleThreadExecutor.submit(MissingPacketConsumer())
    }

    private inner class MissingPacketConsumer : Runnable {
        override fun run() {
            try {
                running = true
                while (true) {
                    consumeMessage()
                    if (queue.size == 0) {
                        synchronized(lock) {
                            // excluding if condition to avoid dead lock on synchronized block
                            lock.wait()
                        }
                    } else {
                        consumeMessage()
                    }
                }
            } catch (e: InterruptedException) {
                e.stackTrace
            } finally {
                running = false
            }
        }

        private fun consumeMessage() {
            val packet = queue.poll()
            if (packet != null) {
                try {
                    Thread.sleep(100)
                    keyEventCharacteristic?.setValue(packet)
                    Log.v("TAG", "poll $packet")
                    for (device in mBluetoothDevices) {
                        gattServer.notifyCharacteristicChanged(
                            device,
                            keyEventCharacteristic,
                            false
                        )
                    }
                } catch (e: Exception) { // ignore, we will try at the next opportunity
                    throw InterruptedException("Packets QUEUE: processing interrupted." + e.message)
                }
            }
        }
    }
}