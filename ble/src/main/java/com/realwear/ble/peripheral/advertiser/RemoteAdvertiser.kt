package com.realwear.ble.peripheral.advertiser

import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import androidx.lifecycle.LiveData

interface RemoteAdvertiser {
    fun advertise(
        advertisementSettings: AdvertiseSettings,
        advertisementData: AdvertiseData,
        advertisementResponse: AdvertiseData
    )
    fun getAdvertisementCallBack() : LiveData<AdvertisementStatus>
    fun stopAdvertising()
}