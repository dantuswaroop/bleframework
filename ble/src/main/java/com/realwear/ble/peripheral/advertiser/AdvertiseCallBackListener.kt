package com.realwear.ble.peripheral.advertiser

interface AdvertiseCallBackListener {
    fun onAdvertisementSuccess()
    fun onAdvertisementFailure(errorCode : Int)
}