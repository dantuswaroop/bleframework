package com.realwear.ble.peripheral

import android.bluetooth.BluetoothGattService
import androidx.lifecycle.LiveData
import java.util.*

interface RemotePeripheralService  {

    fun getServiceUUID() : UUID
    fun getServiceType() : Int

    fun prepareForConnection()

    fun notifyDataChanged(value: String)

    fun disconnect()
    fun getStatus(): LiveData<PeripheralSessionStatus>
}
