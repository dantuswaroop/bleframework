package com.realwear.ble.peripheral.keyboard

import android.bluetooth.BluetoothGattCharacteristic
import com.realwear.ble.Constants
import com.realwear.ble.peripheral.Characteristic
import java.util.*

class IMEOptionsCharacteristic : Characteristic {

    override fun getUUID(): UUID {
        return UUID.fromString(Constants.CHARACTERISTIC_IME_OPTIONS_UUID)
    }

    override fun getProperties(): Int {
        return BluetoothGattCharacteristic.PROPERTY_WRITE
    }

    override fun getPermissions(): Int {
        return BluetoothGattCharacteristic.PERMISSION_WRITE
    }
}