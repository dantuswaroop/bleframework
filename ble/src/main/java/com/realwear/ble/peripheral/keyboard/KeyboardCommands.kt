package com.realwear.ble.peripheral.keyboard

object KeyboardCommands {

    const val CLEAR = "CLEAR"
    const val INPUT_TYPE = "INPUT"
    const val INPUT_IME_OPTION = "IME"
    const val INPUT_TEXT_LENGTH = "LENGTH"

    const val DELIMITER = "|"
}