package com.realwear.ble.peripheral.keyboard

import android.bluetooth.*
import android.content.Context
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.ble.Constants
import com.realwear.ble.peripheral.PeripheralSessionStatus
import com.realwear.ble.peripheral.RemotePeripheralService
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import kotlin.experimental.and

class KeyboardRemotePeripheralService(
    val context: Context,
    val clientConfigDescriptor: ClientCharacteristicConfigDescriptor,
    val userDescriptor: ClientCharacteristicUserDescriptor,
    val keyCharacteristic: KeyCharacteristic,
    val imeOptionsCharacteristic: IMEOptionsCharacteristic
) : RemotePeripheralService {

    private lateinit var imeInputTypeCharacteristic: BluetoothGattCharacteristic
    private lateinit var keyEventCharacteristic: BluetoothGattCharacteristic
    private var mBluetoothDevices: HashSet<BluetoothDevice> = HashSet<BluetoothDevice>()
    private var gattServer: BluetoothGattServer? = null
    private var bluetoothManager: BluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    private lateinit var keyboardService: BluetoothGattService

    private val EXPENDED_ENERGY_FORMAT = BluetoothGattCharacteristic.FORMAT_UINT16
    private val INITIAL_EXPENDED_ENERGY = 0
    private var reliableMessageSender: ReliableMessageSender? = null

    private val TAG: String = "KeyboardRemoteService"

    private val _peripheralStatus = MutableLiveData<PeripheralSessionStatus>()
    private val peripheralStatus = _peripheralStatus as LiveData<PeripheralSessionStatus>

    protected fun addInputReport(inputReport: String?) {
        if (inputReport != null && inputReport.isNotEmpty()) {
            reliableMessageSender?.enqueue(inputReport)
        }
    }

    override fun getStatus(): LiveData<PeripheralSessionStatus> {
        return peripheralStatus
    }

    override fun disconnect() {
        gattServer?.let {
            it.close()
        }
        gattServer = null
    }


    override fun prepareForConnection() {

        keyEventCharacteristic = BluetoothGattCharacteristic(
            keyCharacteristic.getUUID(),
            keyCharacteristic.getProperties(),
            keyCharacteristic.getPermissions()
        )

        val configDescriptor = BluetoothGattDescriptor(
            clientConfigDescriptor.getUUID(),
            clientConfigDescriptor.getPermissions()
        )
        configDescriptor.value = clientConfigDescriptor.getValue()
        keyEventCharacteristic.addDescriptor(configDescriptor)


        val userConfigDescriptor = BluetoothGattDescriptor(
            userDescriptor.getUUID(),
            userDescriptor.getPermissions()
        )
        userConfigDescriptor.value = userDescriptor.getValue()
        keyEventCharacteristic.addDescriptor(userConfigDescriptor)

        keyboardService = BluetoothGattService(
            getServiceUUID(),
            getServiceType()
        )


        imeInputTypeCharacteristic = BluetoothGattCharacteristic(
            imeOptionsCharacteristic.getUUID(),
            imeOptionsCharacteristic.getProperties(),
            imeOptionsCharacteristic.getPermissions()
        )

        keyboardService.addCharacteristic(keyEventCharacteristic)
        keyboardService.addCharacteristic(imeInputTypeCharacteristic)

        gattServer = bluetoothManager.openGattServer(context, gattServerCallback)

        gattServer!!.addService(keyboardService)

        reliableMessageSender = ReliableMessageSender(keyEventCharacteristic, gattServer!!, mBluetoothDevices)
    }


    override fun notifyDataChanged(value: String) {
        Log.v("TAG", "Text Change: notifyDataChanged ${value}")
        addInputReport(value)
    }

    override fun getServiceUUID(): UUID {
        return UUID.fromString(Constants.SERVICE_UUID)
    }

    override fun getServiceType(): Int {
        return BluetoothGattService.SERVICE_TYPE_PRIMARY
    }

    private val gattServerCallback: BluetoothGattServerCallback =
        object : BluetoothGattServerCallback() {
            override fun onConnectionStateChange(
                device: BluetoothDevice,
                status: Int,
                newState: Int
            ) {
                super.onConnectionStateChange(device, status, newState)
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    if (newState == BluetoothGatt.STATE_CONNECTED) {
                        mBluetoothDevices.add(device)
                        Log.v(
                            TAG,
                            "Connected to device: " + device.address + " name " + device.name
                        )
                        _peripheralStatus.postValue(PeripheralSessionStatus.Connected(device))
                    } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                        mBluetoothDevices.remove(device)
                        Log.v(
                            TAG,
                            "Disconnected from device"
                        )
                        _peripheralStatus.postValue(PeripheralSessionStatus.Disconnected())
                    }
                } else {
                    mBluetoothDevices.remove(device)
                    Log.e(
                        TAG,
                        "Error when connecting: $status"
                    )
                    _peripheralStatus.postValue(PeripheralSessionStatus.ErrorInConnecting())
                }
            }

            override fun onCharacteristicReadRequest(
                device: BluetoothDevice, requestId: Int, offset: Int,
                characteristic: BluetoothGattCharacteristic
            ) {
                super.onCharacteristicReadRequest(device, requestId, offset, characteristic)
                Log.d(
                    TAG,
                    "Device tried to read characteristic: " + characteristic.uuid
                )
                Log.d(
                    TAG,
                    "Value: " + Arrays.toString(characteristic.value)
                )
                if (offset != 0) {
                    gattServer!!.sendResponse(
                        device,
                        requestId,
                        BluetoothGatt.GATT_INVALID_OFFSET,
                        offset,  /* value (optional) */
                        null
                    )
                    return
                }
                gattServer!!.sendResponse(
                    device, requestId, BluetoothGatt.GATT_SUCCESS,
                    offset, characteristic.value
                )
            }

            override fun onNotificationSent(device: BluetoothDevice, status: Int) {
                super.onNotificationSent(device, status)
                Log.v(
                    TAG,
                    "Notification sent. Status: $status"
                )
                _peripheralStatus.postValue(PeripheralSessionStatus.NotificationSent())
            }

            override fun onCharacteristicWriteRequest(
                device: BluetoothDevice,
                requestId: Int,
                characteristic: BluetoothGattCharacteristic,
                preparedWrite: Boolean,
                responseNeeded: Boolean,
                offset: Int,
                value: ByteArray
            ) {
                super.onCharacteristicWriteRequest(
                    device, requestId, characteristic, preparedWrite,
                    responseNeeded, offset, value
                )
                Log.v(
                    TAG,
                    "Characteristic Write request: " + Arrays.toString(value)
                )
                val status: Int =
                    writeCharacteristic(characteristic, offset, value)
                _peripheralStatus.postValue(PeripheralSessionStatus.WriteRequestReceived(value))
                if (responseNeeded) {
                    gattServer!!.sendResponse(
                        device, requestId, status,  /* No need to respond with an offset */
                        0,  /* No need to respond with a value */
                        null
                    )
                }
            }

            override fun onDescriptorReadRequest(
                device: BluetoothDevice, requestId: Int,
                offset: Int, descriptor: BluetoothGattDescriptor
            ) {
                super.onDescriptorReadRequest(device, requestId, offset, descriptor)
                Log.d(
                    TAG,
                    "Device tried to read descriptor: " + descriptor.uuid
                )
                Log.d(
                    TAG,
                    "Value: " + Arrays.toString(descriptor.value)
                )
                if (offset != 0) {
                    gattServer!!.sendResponse(
                        device,
                        requestId,
                        BluetoothGatt.GATT_INVALID_OFFSET,
                        offset,  /* value (optional) */
                        null
                    )
                    return
                }
                gattServer!!.sendResponse(
                    device, requestId, BluetoothGatt.GATT_SUCCESS, offset,
                    descriptor.value
                )
            }

            override fun onDescriptorWriteRequest(
                device: BluetoothDevice,
                requestId: Int,
                descriptor: BluetoothGattDescriptor,
                preparedWrite: Boolean,
                responseNeeded: Boolean,
                offset: Int,
                value: ByteArray
            ) {
                super.onDescriptorWriteRequest(
                    device, requestId, descriptor, preparedWrite, responseNeeded,
                    offset, value
                )
                Log.v(
                    TAG,
                    "Descriptor Write Request " + descriptor.uuid + " " + Arrays.toString(value)
                )
                var status = BluetoothGatt.GATT_SUCCESS
                if (descriptor.uuid == clientConfigDescriptor.getUUID()) {
                    val characteristic = descriptor.characteristic
                    val supportsNotifications = characteristic.properties and
                            BluetoothGattCharacteristic.PROPERTY_NOTIFY != 0
                    val supportsIndications = characteristic.properties and
                            BluetoothGattCharacteristic.PROPERTY_INDICATE != 0
                    if (!(supportsNotifications || supportsIndications)) {
                        status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED
                    } else if (value.size != 2) {
                        status = BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH
                    } else if (Arrays.equals(
                            value,
                            BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
                        )
                    ) {
                        status = BluetoothGatt.GATT_SUCCESS
                        _peripheralStatus.postValue(PeripheralSessionStatus.NotificationsDisabled())
                        descriptor.value = value
                    } else if (supportsNotifications &&
                        Arrays.equals(value, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                    ) {
                        status = BluetoothGatt.GATT_SUCCESS
                        _peripheralStatus.postValue(PeripheralSessionStatus.NotificationsEnabled())
                        descriptor.value = value
                    } else if (supportsIndications &&
                        Arrays.equals(value, BluetoothGattDescriptor.ENABLE_INDICATION_VALUE)
                    ) {
                        status = BluetoothGatt.GATT_SUCCESS
                        _peripheralStatus.postValue(PeripheralSessionStatus.NotificationsEnabled())
                        descriptor.value = value
                    } else {
                        status = BluetoothGatt.GATT_REQUEST_NOT_SUPPORTED
                    }
                } else {
                    status = BluetoothGatt.GATT_SUCCESS
                    descriptor.value = value
                }
                if (responseNeeded) {
                    gattServer!!.sendResponse(
                        device, requestId, status,  /* No need to respond with offset */
                        0,  /* No need to respond with a value */
                        null
                    )
                }
            }
        }

    private fun writeCharacteristic(
        characteristic: BluetoothGattCharacteristic?,
        offset: Int,
        value: ByteArray
    ): Int {
        if (offset != 0) {
            return BluetoothGatt.GATT_INVALID_OFFSET
        }
        // Heart Rate control point is a 8bit characteristic
        if (value.size != 1) {
            return BluetoothGatt.GATT_INVALID_ATTRIBUTE_LENGTH
        }
        if ((value[0] and 1) as Int == 1) {
                keyEventCharacteristic?.setValue(
                    INITIAL_EXPENDED_ENERGY,
                    EXPENDED_ENERGY_FORMAT,  /* offset */
                    2
                )
            }
        _peripheralStatus.postValue(PeripheralSessionStatus.WriteCharacteristic(value))
        return BluetoothGatt.GATT_SUCCESS
    }
}