package com.realwear.ble.peripheral.keyboard

import com.realwear.ble.peripheral.Peripheral
import com.realwear.ble.peripheral.advertiser.Advertiser

class KeyboardPeripheral(
    val advertiser: Advertiser,
    val keyboardService: KeyboardPeripheralService
) : Peripheral {

    override fun advertise() {
        advertiser.advertise()
    }

    override fun stopAdvertising() {
        advertiser.stopAdvertising()
    }

    override fun notifyDataChanged(value: Int) {
        keyboardService.notifyDataChanged(""+value)
    }

    override fun prepareForConnection() {
        keyboardService.prepareConnection()
    }

    override fun disconnect() {
        keyboardService.disconnect()
    }

}