package com.realwear.ble.peripheral.advertiser

sealed class AdvertisementStatus {
    class AdvertisementSuccess : AdvertisementStatus()
    data class AdvertisementFailure(val errorCode : Int) : AdvertisementStatus()
    class MultipleAdvertisementIsNotSupported : AdvertisementStatus()
}