package com.realwear.ble.peripheral

import androidx.lifecycle.LiveData

interface PeripheralService {

//    fun getKeyboardClientCharacteristicConfigDescriptor() : KeyboardDescriptor
//    fun getKeyboardUserCharacteristicConfigDescriptor() : KeyboardDescriptor
    fun getStatus() : LiveData<PeripheralSessionStatus>
    fun prepareConnection()
    fun disconnect()
//    fun getKeyboardKeyCharacteristic() : KeyboardCharacteristic
//    fun getKeyboardIMECharacterstic() : KeyboardCharacteristic
    fun notifyDataChanged(value: String)
}
