package com.realwear.ble.peripheral

import java.util.*

interface Descriptor {

    fun getUUID() : UUID
    fun getPermissions() : Int
    fun getValue() : ByteArray
}