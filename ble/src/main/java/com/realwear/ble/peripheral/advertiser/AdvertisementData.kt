package com.realwear.ble.peripheral.advertiser

import android.os.ParcelUuid

interface AdvertisementData {
    fun getIncludeTxPowerLevel() : Boolean
    fun getServiceUUID() : ParcelUuid
    fun includeDeviceName() : Boolean
    fun getRealWearID() : String?
}
