package com.realwear.ble.peripheral.keyboard

import android.bluetooth.BluetoothGattDescriptor
import com.realwear.ble.Constants
import com.realwear.ble.peripheral.Descriptor
import java.util.*

class ClientCharacteristicUserDescriptor(val defaultValue : String) : Descriptor {
    override fun getUUID(): UUID {
        return UUID
            .fromString(Constants.CLIENT_CHARACTERISCIC_USER_DESCRIPTOR_UUID)
    }

    override fun getPermissions(): Int {
        return BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE
    }

    override fun getValue(): ByteArray {
        return defaultValue.toByteArray(charset("UTF-8"))
    }
}