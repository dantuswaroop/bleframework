package com.realwear.ble.peripheral

interface Peripheral {

    fun advertise()

    fun stopAdvertising()

    fun notifyDataChanged(value: Int)

    fun prepareForConnection()

    fun disconnect()
}