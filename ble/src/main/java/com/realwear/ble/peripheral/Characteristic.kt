package com.realwear.ble.peripheral

import java.util.*

interface Characteristic {

    fun getUUID() : UUID
    fun getProperties() : Int
    fun getPermissions() : Int
}

