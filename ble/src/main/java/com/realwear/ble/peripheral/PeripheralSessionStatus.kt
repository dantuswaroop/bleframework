package com.realwear.ble.peripheral

import android.bluetooth.BluetoothDevice

sealed class PeripheralSessionStatus {

    class Connected(val device: BluetoothDevice) : PeripheralSessionStatus()
    class Disconnected : PeripheralSessionStatus()
    class NotificationsEnabled : PeripheralSessionStatus()
    class NotificationsDisabled : PeripheralSessionStatus()
    class WriteRequestReceived(val byteArray: ByteArray) : PeripheralSessionStatus()
    class OnMessageReceived(byteArray: ByteArray) : PeripheralSessionStatus()
    class ErrorInConnecting : PeripheralSessionStatus()
    class NotificationSent : PeripheralSessionStatus()
    class WriteCharacteristic(value: ByteArray) : PeripheralSessionStatus()
}