package com.realwear.ble.peripheral.advertiser.keyboard

import android.os.ParcelUuid
import com.peel.prefs.SharedPrefs
import com.realwear.ble.Constants
import com.realwear.ble.json.AppKeys
import com.realwear.ble.peripheral.advertiser.AdvertisementData
import java.util.*

class KeyboardAdvertisementData : AdvertisementData {
    override fun getIncludeTxPowerLevel(): Boolean {
        return true
    }

    override fun getServiceUUID(): ParcelUuid {
        return ParcelUuid(UUID.fromString(Constants.SERVICE_UUID))
    }

    override fun includeDeviceName(): Boolean {
        return true
    }

    override fun getRealWearID(): String? {
       /* val realWearId = SharedPrefs.get(AppKeys.REALWEAR_ID, null)
        realWearId?.let {
            return it
        } ?: kotlin.run {
            val id = java.lang.Long.toHexString(System.currentTimeMillis())
            SharedPrefs.put(AppKeys.REALWEAR_ID, id)
            return id
        }*/
        return null
    }
}