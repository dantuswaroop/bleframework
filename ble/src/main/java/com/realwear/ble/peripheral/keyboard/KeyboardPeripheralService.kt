package com.realwear.ble.peripheral.keyboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.ble.peripheral.RemotePeripheralService
import com.realwear.ble.peripheral.PeripheralService
import com.realwear.ble.peripheral.PeripheralSessionStatus

class KeyboardPeripheralService(
    val remoteService: RemotePeripheralService
) : PeripheralService {

    private val _peripheralStatus = MutableLiveData<PeripheralSessionStatus>()
    private val peripheralStatus = _peripheralStatus as LiveData<PeripheralSessionStatus>

    override fun getStatus(): LiveData<PeripheralSessionStatus> {
        return peripheralStatus
    }

    override fun notifyDataChanged(value: String) {
        remoteService.notifyDataChanged(value)
    }

    override fun prepareConnection() {
        remoteService.prepareForConnection()
        remoteService.getStatus().observeForever {
            _peripheralStatus.postValue(it)
        }
    }

    override fun disconnect() {
        remoteService.disconnect()
    }
}