package com.realwear.ble.peripheral.advertiser.keyboard

import android.bluetooth.le.AdvertiseSettings
import com.realwear.ble.peripheral.advertiser.AdvertisementSettings

class KeyboardAdvertisementSettings : AdvertisementSettings {
    override fun getAdvertiseMode(): Int = AdvertiseSettings.ADVERTISE_MODE_BALANCED

    override fun getTxPowerLevel(): Int = AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM

    override fun isConnectable(): Boolean {
        return true
    }
}