package com.realwear.ble.peripheral.advertiser.keyboard

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.realwear.ble.R
import com.realwear.ble.peripheral.advertiser.AdvertisementStatus
import com.realwear.ble.peripheral.advertiser.RemoteAdvertiser

/**
 * A BLE based keyboard advertiser
 */
class KeyboardRemoteAdvertiser(context: Context) : RemoteAdvertiser {

    private val TAG: String = "KeyboardRemoteAdvertiser"
    private var bluetoothAdapter: BluetoothAdapter
    private var bluetoothManager: BluetoothManager
    private var bluetoothLeAdvertiser: BluetoothLeAdvertiser? = null

    init {
        bluetoothManager =
            context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter
    }

    // LiveData for reporting connection requests
    private val _advertisementStatus = MutableLiveData<AdvertisementStatus>()
    val advertisementStatus = _advertisementStatus as LiveData<AdvertisementStatus>

    override fun advertise(
        advertisementSettings: AdvertiseSettings,
        advertisementData: AdvertiseData,
        advertisementResponse: AdvertiseData
    ) {
        if (bluetoothAdapter!!.isMultipleAdvertisementSupported) {
            bluetoothLeAdvertiser = bluetoothAdapter!!.bluetoothLeAdvertiser
            bluetoothLeAdvertiser!!.startAdvertising(advertisementSettings,
                advertisementData, advertisementResponse,
                callBack)
        } else {
            _advertisementStatus.postValue(AdvertisementStatus.MultipleAdvertisementIsNotSupported())
        }
    }

    override fun getAdvertisementCallBack(): LiveData<AdvertisementStatus> {
        return advertisementStatus
    }

    private val callBack: AdvertiseCallback = object : AdvertiseCallback() {
        override fun onStartFailure(errorCode: Int) {
            super.onStartFailure(errorCode)
            Log.e(
                TAG,
                "Not broadcasting: $errorCode"
            )
            when (errorCode) {
                ADVERTISE_FAILED_ALREADY_STARTED,
                ADVERTISE_FAILED_DATA_TOO_LARGE,
                ADVERTISE_FAILED_FEATURE_UNSUPPORTED,
                ADVERTISE_FAILED_INTERNAL_ERROR,
                ADVERTISE_FAILED_TOO_MANY_ADVERTISERS -> {
                    _advertisementStatus.postValue(AdvertisementStatus.AdvertisementFailure(errorCode))
                }
                else -> {
                    _advertisementStatus.postValue(AdvertisementStatus.AdvertisementFailure(errorCode))
                }
            }
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
            super.onStartSuccess(settingsInEffect)
            Log.v(TAG, "Broadcasting")
            _advertisementStatus.postValue(AdvertisementStatus.AdvertisementSuccess())
        }
    }

    override fun stopAdvertising() {
        bluetoothLeAdvertiser?.let {
            it.stopAdvertising(callBack)
        }
    }
}