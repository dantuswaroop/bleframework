package com.realwear.ble.peripheral.advertiser

interface AdvertisementSettings {

    fun getAdvertiseMode() : Int
    fun getTxPowerLevel() : Int
    fun isConnectable() : Boolean

}
