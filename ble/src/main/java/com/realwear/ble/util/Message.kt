package com.realwear.ble.util

data class Message(
    val start: Int,
    val before: Int,
    val difference: String
) {
}