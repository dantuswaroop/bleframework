package com.realwear.ble.util

import java.lang.StringBuilder

object MessageEncoder {

    private const val MESSAGE_DELIMITER = "|"
    private const val DATA_MAX_LENGTH = 19

    fun getEncodedMessage(start: Int, before: Int, difference: String): List<String> {
        val encodedMessages = mutableListOf<String>()
        var sentIndex = 0
        do {
            val stringBuilder = StringBuilder()
//            val startHex = if (sentIndex == 0) {
//                String.format("%03X", start)
//            } else {
//                String.format("%03X", start + sentIndex)
//            }
//            val beforeHex = if (sentIndex == 0) {
//                String.format("%03X", before)
//            } else {
//                String.format("%03X", 0)
//            }
//            stringBuilder.append(startHex)
//            stringBuilder.append(MESSAGE_DELIMITER)
//            stringBuilder.append(beforeHex)
//            stringBuilder.append(MESSAGE_DELIMITER)
            if (sentIndex + DATA_MAX_LENGTH >= difference.length) {
                stringBuilder.append(difference.substring(sentIndex))
            } else {
                stringBuilder.append(difference.substring(sentIndex, sentIndex + DATA_MAX_LENGTH))
            }
            encodedMessages.add(stringBuilder.toString())
            sentIndex += DATA_MAX_LENGTH
        } while (sentIndex < difference.length)
        encodedMessages.add(MESSAGE_DELIMITER)
        return encodedMessages
    }

    fun getDecodedMessage(encodedString : String): Message {
        val split = encodedString.split(MESSAGE_DELIMITER)
        if(split.size == 3) {
            val start = split[0].toInt(16)
            val before = split[1].toInt(16)
            val difference = split[2]
            return Message(start, before, difference)
        }
        return Message(-1,-1, "")
    }
}