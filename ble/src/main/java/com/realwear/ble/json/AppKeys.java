package com.realwear.ble.json;

import com.peel.prefs.TypedKey;

/**
 * Common keys used throughout the app to access data from AppScope.
 */
public class AppKeys {

    public static final TypedKey<String> CONNECTED_BT_ID = new TypedKey<>("connectedBTID", String.class);
    public static final TypedKey<String> REALWEAR_ID = new TypedKey<>("realwearID", String.class);

}
